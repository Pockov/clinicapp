<?php
require_once("connection.php");
if(!empty($_POST["email"])) {
	$pdo_statement=$conn->prepare("update patients set name='" . $_POST[ 'name' ] . "', email='" . $_POST[ 'email' ]. "' , phone='" . $_POST[ 'phone' ]. "', address='" . $_POST[ 'address' ]. "', medicalConditon='" . $_POST[ 'medicalConditon' ]. "', bloodType='" . $_POST['bloodType']. "' where id=" . $_GET["id"]);
	$result = $pdo_statement->execute();
	if($result) {
		header('location:index.php');
	}
}
$pdo_statement = $conn->prepare("SELECT * FROM patients where id=" . $_GET["id"]);
$pdo_statement->execute();
$result = $pdo_statement->fetchAll();
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>

<body class="m-3">

    
    <div class="container">
        <div class="container-fluid">

            <form action="#" method="post">
                <div class="mb-3">
                    <label class="form-label">Name</label>
                    <input type="text" class="form-control" name="name" value="<?php echo $result[0]['name']; ?>">
                </div>
                <div class="mb-3">
                    <label class="form-label">Email</label>
                    <input type="email" class="form-control" name="email" value="<?php echo $result[0]['email']; ?>">
                </div>
                <div class="mb-3">
                    <label class="form-label">Phone</label>
                    <input type="text" class="form-control" name="phone" value="<?php echo $result[0]['phone']; ?>">
                </div>
                <div class="mb-3">
                    <label class="form-label">Address</label>
                    <input type="text" class="form-control" name="address" value="<?php echo $result[0]['address']; ?>">
                </div>
                <div class="mb-3">
                    <label class="form-label">Medical Condition</label>
                    <input type="text" class="form-control" name="medicalConditon" value="<?php echo $result[0]['medicalConditon']; ?>">
                </div>
                <div class="mb-3">
                    <label class="form-label">Blood Type</label>
                    <input type="text" class="form-control" name="bloodType" value="<?php echo $result[0]['bloodType']; ?>">
                </div>
                
                <button type="submit" name="login" class="btn btn-primary">Edit</button>
            </form><br>
            <button class="btn btn-primary"><a style="color:white; text-decoration:none;" href="index.php">Back</a></button>
            </div>
        </div>
</body>

</html>
