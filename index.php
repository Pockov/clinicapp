<?php

session_start();

include("connection.php");


if(isset($_SESSION['email'])) {
echo "Welcome <strong>".$_SESSION['email']."</strong><br><br>";
} else {
header('location: login.php');
}

$sql = "SELECT * FROM patients";

try{
 $pdo = new PDO($dsn, $username, $password);
 $stmt = $pdo->query($sql);

 if($stmt === false){
  die("Error");
 }

}catch (PDOException $e){
  echo $e->getMessage();
}

?>

<?php


?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>

<body class="m-4">
    <div class="container">
        <div class="input-group mb-3">
            <button class="btn btn-primary" type="button" id="searchBtn">Search</button>
            <input type="text" class="form-control" id="search" name="search">
        </div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Phone</th>
                    <th scope="col">Address</th>
                    <th scope="col">Medical Condition</th>
                    <th scope="col">BloodType</th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <?php while($row = $stmt->fetch(PDO::FETCH_ASSOC)) : ?>
                <tr>
                    <td><?php echo htmlspecialchars($row['id']); ?></td>
                    <td><?php echo htmlspecialchars($row['name']); ?>
                    <td><?php echo htmlspecialchars($row['email']); ?>
                    <td><?php echo htmlspecialchars($row['phone']); ?>
                    <td><?php echo htmlspecialchars($row['address']); ?>
                    <td><?php echo htmlspecialchars($row['medicalConditon']); ?>
                    <td><?php echo htmlspecialchars($row['bloodType']); ?></td>
                    <td><button class="btn btn-primary"><a style="color:white; text-decoration: none;"
                                href='edit.php?id=<?php echo $row['id']; ?>'>Edit</a></button></td>
                    <td><button class="btn btn-primary"><a style="color:white; text-decoration: none;"
                                href='delete.php?id=<?php echo $row['id']; ?>'>Delete</a></button></td>

                </tr>
                <?php endwhile; ?>
            </tbody>
        </table>
        <button class="btn btn-primary"><a href="logout.php"
                style="color:white; text-decoration: none;">Logout</a></button>
    </div>

    <div id="searchResult"></div>

    <script>
        $(document).ready(function(){
            $("#search").keyup(function(){
                var input = $(this).val();
                // alert(input);

                if(input !=""){
                    $.ajax({
                        url:"livesearch.php",
                        method: "POST",
                        data:{input:input},
                        
                        success:function(data){
                            $("#searchResult").html(data);
                        }
                    })
                }else{
                    $("#searchResult").css("display","none");
                }
            })
        })
    </script>
</body>

</html>