<?php
session_start();
if(!empty($_SESSION['email'])) {
header('location:index.php');
}
require 'connection.php';

if(isset($_POST['login'])) {

$email = $_POST['email'];
$pass = $_POST['password'];

if(empty($email) || empty($pass)) {
$message = 'All field are required';
} else {
$query = $conn->prepare("SELECT email, password FROM admin WHERE 
email=? AND password=? ");
$query->execute(array($email,$pass));
$row = $query->fetch(PDO::FETCH_BOTH);

if($query->rowCount() > 0) {
  $_SESSION['email'] = $email;
  header('location:index.php');
} else {
  $message = "Email/Password is wrong";
}
}
}
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>

<body class="m-3">

    <?php
if(isset($message)) {
echo $message;
}
?>
    <div class="container">
        <div class="container-fluid">

            <form action="#" method="post">
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Email</label>
                    <input type="email" class="form-control" name="email">
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Password</label>
                    <input type="password" class="form-control" name="password">
                </div>
                
                <button type="submit" name="login" class="btn btn-primary">Log In</button>
            </div>
        </div>
    </form>
</body>

</html>
